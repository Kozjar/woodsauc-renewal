const ENDPOINTS = {
  TWITCH_AUTH: '/api/twitch/auth',
  TWITCH_REWARDS: '/api/twitch/rewards',
  DA_AUTH: '/api/donationalerts/auth',
  TWITCH_CHANNEL_POINTS: '/api/twitch/channelPoints',
  USER: {
    USERNAME: '/api/username',
    AUC_SETTINGS: '/api/aucSettings',
    SETTINGS: '/api/settings',
    DATA: '/api/user/userData',
    INTEGRATION: '/api/integration',
  },
  RANDOM: {
    INTEGER: '/api/random/integer',
  },
};

export default ENDPOINTS;
